package com.micro.weatherservice.api;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.micro.weatherservice.dto.WeatherDTO;
import com.micro.weatherservice.service.WeatherService;

@RestController
@RequestMapping("/weather")
public class WeatherAPI {
	
	@Autowired
	WeatherService weatherService;
	
	@RequestMapping(value = "/{userName}", method = RequestMethod.GET)
	public List<List<WeatherDTO>> getWeatherForUser(@PathVariable("userName") final String userName){
		
		return weatherService.getLocationDetailsFromDatabase(userName).stream().map( location ->
		{
			return weatherService.getWeatherDetailsFromInternet(location.getWoeid());
		}).collect(Collectors.toList());
	}

}
