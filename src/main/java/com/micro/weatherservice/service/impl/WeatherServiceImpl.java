package com.micro.weatherservice.service.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.micro.weatherservice.dto.LocationDTO;
import com.micro.weatherservice.dto.WeatherDTO;
import com.micro.weatherservice.service.WeatherService;

@Service
public class WeatherServiceImpl implements WeatherService {

	
	@Autowired
	RestTemplate restTemplate;
	
	@Override
	public List<LocationDTO> getLocationDetailsFromDatabase(String userName) {

		ResponseEntity<List<LocationDTO>> locationResponse = restTemplate
				.exchange("http://database/back/location/"+userName,HttpMethod.GET,null, new ParameterizedTypeReference<List<LocationDTO>>() {});
		return locationResponse.getBody();
	}

	@Override
	public List<LocationDTO> getLocationDetailsFromInternet(String locationTitle) {
		ResponseEntity<List<LocationDTO>> locationResponse = restTemplate
				.exchange("https://www.metaweather.com/api/location/search/?query="+locationTitle,
						HttpMethod.GET,null,
						new ParameterizedTypeReference<List<LocationDTO>>() {});
		return locationResponse.getBody();
	}

	@Override
	public List<LocationDTO> setLocationDetailsToDatabase(LocationDTO locationDTO) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<WeatherDTO> getWeatherDetailsFromInternet(Integer woeId) {
		Date date = new Date();
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		int year = cal.get(Calendar.YEAR);
		int month = cal.get(Calendar.MONTH);
		int day = cal.get(Calendar.DAY_OF_MONTH);
		
		ResponseEntity<List<WeatherDTO>> locationResponse = restTemplate.exchange("https://www.metaweather.com/api/location/"+woeId+"/"+year+"/"+(month+1)+"/"+day+"/",HttpMethod.GET,null, new ParameterizedTypeReference<List<WeatherDTO>>() {});
		return locationResponse.getBody();
	}

}
