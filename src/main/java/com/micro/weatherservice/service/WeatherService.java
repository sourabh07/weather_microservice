package com.micro.weatherservice.service;

import java.util.List;

import com.micro.weatherservice.dto.LocationDTO;
import com.micro.weatherservice.dto.WeatherDTO;


public interface WeatherService {

	public List<LocationDTO> getLocationDetailsFromDatabase(String userName);
	
	public List<LocationDTO> getLocationDetailsFromInternet(String locationTitle);
	
	public List<LocationDTO> setLocationDetailsToDatabase(LocationDTO locationDTO);
	
	public List<WeatherDTO> getWeatherDetailsFromInternet(Integer woeId);
	
	
}
